import axios from "axios";

const API_URL = `https://ddm-project.herokuapp.com/api/2/all`;

export const FETCH_ADVERTISEMENTS = "FETCH_ADVERTISEMENTS";

export function fetchAdvertisements() {
  const request = axios.get(API_URL);
  return dispatch => {
    request
      .then(response => {
        dispatch({ type: FETCH_ADVERTISEMENTS, payload: response.data });
      })
      .catch(err => console.log(err));
  };
}
