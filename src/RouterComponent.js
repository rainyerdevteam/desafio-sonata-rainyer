import React, { Component } from "react";
import { Scene, Router } from "react-native-router-flux";
import AdvertisementsList from "./components/Advertisements/AdvertisementsList";
import AdvertisementDetails from "./components/Advertisements/AdvertisementDetails";

export default () => (
  <Router>
    <Scene key="root">
      <Scene
        key="advertisements"
        component={AdvertisementsList}
        title="Todos os anúncios"
        initial
      />
      <Scene
        key="advertisementDetails"
        component={AdvertisementDetails}
        title="Detalhes do anúncio"
      />
    </Scene>
  </Router>
);
