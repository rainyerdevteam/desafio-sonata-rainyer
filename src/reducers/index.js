import { combineReducers } from "redux";
import AvertisementsReducer from "./advertisementsReducer";

const rootReducer = combineReducers({
  advertisements: AvertisementsReducer
});

export default rootReducer;
