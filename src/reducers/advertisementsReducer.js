import { FETCH_ADVERTISEMENTS } from "../actions/index";

export default function(state = [], action) {
  switch (action.type) {
    case FETCH_ADVERTISEMENTS:
      return [...action.payload, ...state];
  }
  return state;
}
