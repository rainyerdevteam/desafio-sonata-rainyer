import React, { Component } from "react";
import RouterComponent from "./RouterComponent";
import { Provider } from "react-redux";
import store from "./configureStore";

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <RouterComponent />
      </Provider>
    );
  }
}
