import React, { Component } from "react";
import { Text, View, StyleSheet, Image, ScrollView } from "react-native";
import { Badge } from "react-native-elements";

export default class AdvertisementDetails extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { advertisement } = this.props;
    return (
      <ScrollView style={styles.container}>
        <Image
          style={styles.advImage}
          source={{
            uri: advertisement.picture
          }}
        />
        <View>
          <View>
            <Text style={styles.title}>{advertisement.title}</Text>
          </View>

          <View>
            <Text style={styles.description}>{advertisement.description}</Text>
          </View>

          <View style={styles.badges}>
            <Badge containerStyle={styles.priceBadge}>
              <Text style={styles.priceText}>
                Preço: R$ {advertisement.price || "0.00"}
              </Text>
            </Badge>

            <Badge containerStyle={styles.amountBadge}>
              <Text style={styles.amountText}>
                Quantidade: {advertisement.amount || "0"}
              </Text>
            </Badge>
          </View>
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#470098"
  },
  advImage: {
    height: 200
  },
  title: {
    color: "white",
    fontSize: 25,
    marginTop: 5,
    marginBottom: 10,
    textAlign: "center",
    fontWeight: "bold"
  },
  description: {
    color: "white",
    textAlign: "center",
    fontSize: 17,
  },
  priceBadge: {
    backgroundColor: "violet",
    width: 150
  },
  priceText: {
    color: "white",
    fontWeight: "bold"
  },
  amountBadge: {
    backgroundColor: "violet",
    width: 150
  },
  amountText: {
    color: "white",
    fontWeight: "bold"
  },
  badges: {
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-evenly"
  }
});