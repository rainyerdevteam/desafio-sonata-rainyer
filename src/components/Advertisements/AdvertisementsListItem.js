import React, { Component } from "react";
import { Text, View, TouchableWithoutFeedback, StyleSheet } from "react-native";
import { Card, ListItem, Button, Icon } from "react-native-elements";
import { Actions } from "react-native-router-flux";

export default class AdvertisementsListItem extends Component {
  constructor(props) {
    super(props);
    this.onPress = this.onPress.bind(this);
  }
  onPress() {
    const { advertisement } = this.props;
    const sceneTitle = advertisement.title;
    Actions.advertisementDetails({
      advertisement: advertisement
    });
  }

  render() {
    const { advertisement } = this.props;
    return (
      <TouchableWithoutFeedback onPress={this.onPress}>
        <Card
          title={advertisement.title}
          image={{ uri: advertisement.picture }}
          containerStyle={styles.card}
          titleStyle={styles.cardTitle}
        >
          <Text numberOfLines={3} style={styles.descriptionText}>
            {advertisement.description}
          </Text>
        </Card>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  card: { borderRadius: 10, borderWidth: 1, marginBottom:20 },
  cardTitle: {
    fontSize: 20
  },
  descriptionText: {
    flex: 1,
    marginBottom: 10,
    padding: 5
  }
});
