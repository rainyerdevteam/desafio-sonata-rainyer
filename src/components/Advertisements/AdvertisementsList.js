import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchAdvertisements } from "../../actions/index";
import AdvertisementsListItem from "./AdvertisementsListItem";

class AdvertisementsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      advertisements: []
    };
  }

  componentWillMount = () => {
    this.props.fetchAdvertisements();
  };

  componentWillReceiveProps(nextProps) {
    this.props.advertisements = nextProps.advertisements;
  }

  showLoading() {
    return <ActivityIndicator size="large" color="#fff" />;
  }

  renderListItem({ item }) {
    return <AdvertisementsListItem advertisement={item} />;
  }
  render() {
    const advertisements = this.props.advertisements || [];
    return (
      // TODO: adequate show loading to the fetchAdvertisements request
      <View style={styles.listContainer}>
        {advertisements.length ? (
          <FlatList
            data={advertisements}
            renderItem={this.renderListItem}
            keyExtractor={(item, index) => index.toString()}
          />
        ) : (
          this.showLoading()
        )}
      </View>
    );
  }
}

const mapStateToProps = ({ advertisements }) => {
  return { advertisements };
};

const styles = StyleSheet.create({
  listContainer: {
    flex: 1,
    backgroundColor: "#470098",
    justifyContent: "center"
  }
});

export default connect(
  mapStateToProps,
  { fetchAdvertisements }
)(AdvertisementsList);
